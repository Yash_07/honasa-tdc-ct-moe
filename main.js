const moment = require("moment");
const axios = require('axios');



function sleep(ms) {
    console.log('sleeping for :>> ', ms);
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}


const getMoeReadyObject = (userData, eventData) => {
    const customerAttribute = {
        "type": "customer",
        "customer_id": userData['mobile'],
        "attributes": userData
    }

    const eventAttribute = {
        "type": "event",
        "customer_id": userData['mobile'],
        "actions": [
            {
                "action": "charged",
                "attributes": eventData,
                "current_time": eventData["current_time"],
                "user_time": eventData["current_time"],
				"user_timezone_offset": eventData["user_timezone_offset"],
                "platform" : eventData['platform']
            }
        ]
    }

    return {
        "type": "transition",
        "elements": [customerAttribute, eventAttribute]
    }
}



const sendToMoe = async (paylaod) => {
    try{
        var data = JSON.stringify(paylaod);

        var config = {
            method: 'post',
            url: 'https://api.moengage.com/v1/transition/2JZQ3J0KASVVABHEN12EXPV7?app_id=2JZQ3J0KASVVABHEN12EXPV7', // appID of main account
            headers: { 
                'Content-Type': 'application/json', 
                'Authorization': 'Basic MkpaUTNKMEtBU1ZWQUJIRU4xMkVYUFY3OmtXYnotRDVwQnFJSktNWWpCMWpAVHRoMQ=='
            },    
            data : data
        };
    

        // var config = {
        //     method: 'post',
        //     url: 'https://api.moengage.com/v1/transition/UKA6LOAR56FL8SI1JILW1YQC_DEBUG?app_id=UKA6LOAR56FL8SI1JILW1YQC_DEBUG', // appID of main account
        //     headers: { 
        //         'Content-Type': 'application/json', 
        //         'Authorization': 'Basic VUtBNkxPQVI1NkZMOFNJMUpJTFcxWVFDX0RFQlVHOkwwZ2dPQXVvMDg3RzhIOTN3SEFULXZQeA=='
        //     },    
        //     data : data
        // };
        const response = await axios(config)

        console.log('response.data :>> ', response.data);

    } catch (e) {
        await sleep(1000)
    }
   
}

const fixPhoneNo = (ph) => {
    let phone = String(ph)
    if(phone.length <= 10){
        return phone;
    }
    return phone.slice(phone.length - 10);
}

const fixDate = (date) => {
   return moment.unix(Number(date.slice(3))).toISOString()
}
const fixDateEpoch = (date) => {
    return moment.unix(Number(date.slice(3)))
 }





const getCursor = async (startDate, endDate) => {
        const data = JSON.stringify({
            "event_name": "Charged",
            "from": Number(startDate),
            "to": Number(endDate)
        });
    
        const config = {
            method: 'post',
            url: 'https://api.clevertap.com/1/events.json?events=false&batch_size=100',
            headers: { 
                'X-CleverTap-Account-Id': '48K-W6K-5Z6Z',       // Account Id of Main CT account
                'X-CleverTap-Passcode': 'AYM-QIB-UHUL',  // Password of Main CT account
                'Content-Type': 'application/json'
            },
            data : data
        };
    
        const response = await axios(config);
        const resData = response.data
        return resData.cursor
    }
    

const getData = async (cursor) => {

    const config = {
        method: 'get',
        url: `https://api.clevertap.com/1/events.json?cursor=${cursor}`,
        headers: { 
            'X-CleverTap-Account-Id': '48K-W6K-5Z6Z',       // Account Id of Main CT account
            'X-CleverTap-Passcode': 'AYM-QIB-UHUL',  // Password of Main CT account
            'Content-Type': 'application/json'
        }
    };

    const response = await axios(config);
    const ctData = response.data;
    return ctData;
}


const main = async () => {
        let count = 0;
        const startDate = moment("15/10/2021", "DD/MM/YYYY").format("YYYYMMDD");
        const endDate = moment("28/02/2022", "DD/MM/YYYY").format("YYYYMMDD");;
    
        console.log('startDate :>> ', startDate);
        console.log('endDate :>> ', endDate);
    
        let cursor = await getCursor(startDate, endDate);
       // console.log('cursor :>> ', cursor);
        cursorCount = 0;
        while(cursor){
            try{
               // console.log("\tCount: ", count);
                const ctData = await getData(cursor);
                //console.log('ctData :>> ', Object.keys(ctData));
                const nextCursor = ctData['next_cursor'] ? ctData['next_cursor'] : null;
                const records = ctData['records'] ? ctData['records'] : null
               // console.log('nextCursor :>> ', nextCursor);
                records ? records.forEach( async record => {
                    try{
                        const eventData = record['event_props'] ? record['event_props'] : null;
                        // console.log('eventData :>> ', eventData);
                        // eventData['user_time'] = moment.unix(eventData['timeStamp'] / 1000).toISOString();
                        //console.log(eventData['user_time'])

                        //console.log(eventData['current_time'])
                        eventData['current_time'] = eventData['timeStamp'] && eventData['timeStamp'].length === 13 ? moment(Number(eventData['timeStamp'])).unix()  :  moment(eventData['timeStamp'] || fixDateEpoch(eventData['Date'])).unix();
                        //console.log(eventData['current_time'])
                        //console.log(eventData['user_time'])
                        // eventData['purchaseDate'] =  eventData['user_time']
                        // eventData['purchaseAmount'] = eventData['revenue']
                        // eventData['orderNumber'] = eventData['id'] && eventData['orderNumber']
                        //console.log(eventData['orderNumber'])
                        delete eventData['id']
                        delete eventData['revenue']
                        eventData['user_timezone_offset'] = 19800
                        let profileData = (record['profile'] && record['profile']['profileData']) || {};
                        const contactNumber = record['profile'] && record['profile']['phone'] && fixPhoneNo(record['profile']['phone'])
                        
                        profileData['name'] = record['profile'] && record['profile']['name']
                        profileData['email'] = record['profile'] && record['profile']['email']
                        
                        profileData['mobile'] = contactNumber
                        
                        profileData['first_name'] = profileData['firstname']
                        profileData['last_name'] = profileData['lastname']
                        //console.log(profileData['createdat'])

                        profileData['created_time'] = eventData['user_time'] 
                        //console.log(profileData['created_time'])
                        profileData['lastPurchaseDate'] = profileData['last_purchase_date'] && fixDate(profileData['last_purchase_date'])
                        profileData['Postcode'] = profileData['pincode'] || profileData['postcode']
                        profileData['State'] = profileData['state']
                        profileData['City'] = profileData['city']
                        profileData['currentAmountInCart'] = profileData['current_amount_incart']
                        profileData['lastWalletBalance'] = profileData['lastwalletbalance']
                        profileData['lastViewedCategory'] = profileData['lastviewedcategory']
                        profileData['lastViewedShortName'] = profileData['shortnameprop']
                        profileData['lastViewedProductName'] = profileData['productnameprop']
                        profileData['lastViewedProductUrl'] = profileData['producturlprop']
                        delete profileData['last_purchase_date']
                        delete profileData['postcode']
                        delete profileData['pincode']
                        delete profileData['state']
                        delete profileData['city']
                        delete profileData['current_amount_incart']
                        delete profileData['lastwalletbalance']
                        delete profileData['lastviewedcategory']
                        delete profileData['shortnameprop']
                        delete profileData['productnameprop']
                        delete profileData['producturlprop']
                        //console.log(profileData);
                        // console.log(eventData)
                        const moeObject = getMoeReadyObject(profileData, eventData);
                        console.log( JSON.stringify(moeObject));
                        //console.log('eventData ID :>> ', eventData['orderNumber']);
                        // console.log('\tprofileData prodile :>> ', profileData['mobile']);
                        console.log("\n");
                       await sendToMoe(moeObject);
                    } catch (error) { 
                        // console.log('error :>> ', error);
                        throw error
                    }
                }) : null;
                cursor = nextCursor;
            } catch (err){
                console.log('error :>> ', error.data);
               await sleep(600000);
            }
            

        }}
 main();
